#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from . import *

__appname__     = "test"
__author__      = "Mark Guzdial"
__copyright__   = ""
__credits__     = ["Mark Guzdial",  "Marco Sirabella"]    # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "marco@sirabella.org"
__status__      = "Development"  # "Prototype", "Development" or "Production"
__module__      = ""


def clearTopHalf1(pic):
    h = getHeight(pic)
    for pixel in getPixels(pic):
        y = getY(pixel)
        if y < h/2:
            setColor(pixel, white)


def clearTopHalf2(pic):
    all = getPixels(pic)
    for index in range(0, len(all)/2):
        pixel = all[index]
        setColor(pixel, white)


def clearTopHalf3(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    for y in range(0, h/2):
        for x in range(0, w):
            pixel = getPixel(pic, x, y)
            setColor(pixel, white)


def clearTopHalf3b(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    for x in range(0, w):
        for y in range(0, h/2):
            pixel = getPixel(pic, x, y)
            setColor(pixel, white)


def copyAThird1(pic):
    h = getHeight(pic)
    for pixel in getPixels(pic):
        x = getX(pixel)
        y = getY(pixel)
        if y < h/3:
            targetPixel = getPixel(pic, x, y+(2*h/3))
            setColor(targetPixel, getColor(pixel))


def copyAThird2(pic):
    all = getPixels(pic)
    h = getHeight(pic)
    for index in range(0, len(all)/3):
        pixel = all[index]
        color = getColor(pixel)
        x = getX(pixel)
        y = getY(pixel)
        targetPixel = getPixel(pic, x, y+(2*h/3))
        setColor(targetPixel, color)


def copyAThird3(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    for x in range(0, w):
        for y in range(0, h/3):
            pixel = getPixel(pic, x, y)
            color = getColor(pixel)
            # Copies
            targetPixel = getPixel(pic, x, y+(2*h/3))
            setColor(targetPixel, color)


def copyAThird3b(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    for x in range(0, w):
        for y in range(0, h/3):
            pixel = getPixel(pic, x, y)
            color = getColor(pixel)
            # Mirrors instead of copies
            targetPixel = getPixel(pic, x, h-y-1)
            setColor(targetPixel, color)


def copyAThird4(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    for x in range(0, w):
        for y in range(2*h/3, h):
            targetPixel = getPixel(pic, x, y)
            srcPixel = getPixel(pic, x, y-(2*h/3))
            setColor(targetPixel, getColor(srcPixel))


def copyAThirdEmpty(pic):
    h = getHeight(pic)
    w = getWidth(pic)
    canvas = makeEmptyPicture(w, h)
    for x in range(0, w):
        for y in range(0, h/3):
            pixel = getPixel(pic, x, y)
            color = getColor(pixel)
            targetPixel = getPixel(canvas, x, y)
            setColor(targetPixel, color)
            targetPixel = getPixel(canvas, x, y+(2*h/3))
            setColor(targetPixel, color)
    explore(canvas)
