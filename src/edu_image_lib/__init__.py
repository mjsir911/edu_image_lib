#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import image

__appname__     = "__init__"
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella"]    # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "marco@sirabella.org"
__status__      = "Development"    # "Prototype",  "Development" or "Production"
__module__      = "edu_image_lib"


# Colors

class Color():
    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

    def from_hex(code):
        red   = code >> 8 * 2 & 0xFF
        green = code >> 8 * 1 & 0xFF
        blue  = code >> 8 * 0 & 0xFF
        return Color(red, green, blue)

    def __str__(self):
        return '{}, color={} r={} g={} b={}'.format(type(self).__name__,
                                                    'color',
                                                    self.red,
                                                    self.green,
                                                    self.blue)


white = Color.from_hex(0xFFFFFF)


def getColor(pixel):
    """
    getColor  takes  a  pixel  as  input  and  returns  a  Color  object with
    the  color  at  that  pixel
    """
    return Color(pixel.getRed(), pixel.getGreen(), pixel.getBlue())


def setColor(px, color):
    """
    setColor  takes  a  pixel  as  input and a  Color,  then  sets the  pixel
    to  that  color
    """
    px.setRed(color.red)
    px.setGreen(color.green)
    px.setBlue(color.blue)
    px.img.updatePixel(px)


def makeColor(r, g, b):
    """
    makeColor  takes  red,  green,  and  blue  values  (in  that order)
    between  0  and  255,  and  returns  a  Color  object
    """
    return Color(r, g, b)


def pickAColor():
    """
    pickAColor  lets  you  use  a  color  chooser  and  returns  the chosen
    color
    """
    raise NotImplementedError()


def getHeight(pic):
    return pic.getHeight()


def getPixels(pic):
    lp = []
    for px in pic.getPixels():
        px.img = pic
        lp.append(px)
    return lp


def getY(px):
    return px.getY()


def getX(px):
    return px.getX()


def getWidth(pic):
    return pic.getWidth()


def getPixel(pic, x, y):
    ret = pic.getPixel(x, y)
    ret.img = pic
    return ret


def makePicture(filepath):
    return image.Image(filepath)


def makeEmptyPicture(w, h):
    return image.EmptyImage(w, h)


def explore(img):
    win = image.ImageWin(img.getWidth(), img.getHeight())
    img.draw(win)


show = repaint = explore
